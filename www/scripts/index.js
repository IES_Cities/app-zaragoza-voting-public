/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership. The ASF licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
var app = {
	initialize : function() {
		this.bindEvents();
	},
	bindEvents : function() {
		document.addEventListener('deviceready', this.onDeviceReady, false);
	},
	onDeviceReady : function() {
		app.receivedEvent('deviceready');

		// Handle backbutton press to avoid app closing
		document.addEventListener("backbutton", onBackKeyDown, true);

	},
	receivedEvent : function(id) {
		// login control
	}
};

function isWebKit() {
	return /WebKit/.test(navigator.userAgent);
}

function isMobile() {
	return /(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/
			.test(navigator.userAgent);
}

function isFirefox() {
	return /Firefox/.test(navigator.userAgent);
}

var tapped = false;

var hideKeyboard = function() {
	document.activeElement.blur();
	$("input").blur();
};


function onConfirmExitApp(buttonIndex) {
	if (buttonIndex == 2) {
		navigator.app.exitApp();
	}
}

function onBackKeyDown() {

    var volver;

    if ($('.volverCategoria:visible').size()) {
        if ($('.volverCategoria:visible').parent().parent().position().top >= 0) {
            volver = $('.volverCategoria:visible');
        }

    };

    $('.volver:visible').each(function() {
        if ($(this).parent().parent().position().top >= 0) {
            volver = this;
            return false;
        };
    });


    configPosicionTop = $('#configuracion').position().top;
    newUserPosicionTop = $('#new_user').position().top;
    if (!volver && configPosicionTop < 0 && newUserPosicionTop < 0) {
    	console.log("SALIR DE LA APP!!");
    	navigator.notification.confirm('¿Desea salir de la aplicación?', 
    	onConfirmExitApp, 
    	'Cerrar aplicación', 
    	'No,Salir' 
    	);
    };


    $(volver).click();

    /*	navigator.notification.confirm('¿Desea salir de la aplicación?', 
    	onConfirm, 
    	'Cerrar aplicación', 
    	'No,Salir' 
    	);*/
}


// EVENTOS DE TECLADO
window.addEventListener('native.keyboardshow', keyboardShowHandler);

function keyboardShowHandler(e){
	$('.voteOption').hide();
    $('.botonera').hide();

	
    
    $("#details .contenido").height("100%");
    // $("#details .contenido").scrollTop($("#newaportacion textarea").offset().top);


}

window.addEventListener('native.keyboardhide', keyboardHideHandler);

function keyboardHideHandler(e){
	console.log(e);
    $('.voteOption').show();
    $('.botonera').show();

    $("#details .contenido").height(app.alturaContenidoDetalle);
    $("#details .contenido").scrollTop($("#newaportacion textarea").offset().top);

}


