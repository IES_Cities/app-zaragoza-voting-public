'use strict';

/* Controllers */
var iescitiesAppControllers = angular.module('iescitiesAppControllers', ["checklist-model"]);

/** Main controller */
// FIXME: Sacar controlador de main.html y crear los necesarios
iescitiesAppControllers
    .controller(
        'MainCtrl', [
            '$rootScope',
            '$scope',
            'geolocation',
            function($rootScope, $scope, geolocation) {

            }
        ]);

/** Poll List controller */
iescitiesAppControllers.controller('menuCtrl', ['$rootScope', '$scope', 'ApiService', 'votingService',
    function($rootScope, $scope, ApiService, votingService) {

        // recoger de almacenamiento local los barrios seleccionados
        // si no existen barrios seleccionar todos
        $rootScope.barriosZaragoza = [
            'Toda la ciudad',
            'Actur',
            'Miralbueno',
            'Almozara',
            'Arrabal',
            'Delicias',
            'Casco Historico',
            'Oliver-Valdefierro',
            'Centro',
            'Universidad',
            'Las Fuentes',
            'San Jose',
            'Casablanca',
            'Torrero'
        ];

        // OBJETO USUARIO

        $rootScope.barriosFiltrados = {
            availableOptions: ['Toda la ciudad'],
            selectedOption: 'Toda la ciudad'
        };
        $rootScope.barriosFiltrados.availableOptions = $rootScope.barriosZaragoza;

        if (!window.localStorage.getItem("barrioSeleccionado")) {
            window.localStorage.setItem("barrioSeleccionado", JSON.stringify($rootScope.barriosFiltrados.selectedOption));
        };


        $rootScope.barriosFiltrados.selectedOption = JSON.parse(window.localStorage.getItem("barrioSeleccionado"));
        console.log("FILTRO BARRIO " + $rootScope.barriosFiltrados.selectedOption);

        $rootScope.estaCategoriaAbierta = false; // para la gestion de la flecha volver en el menu

        // EXTRAER TAG DE PROPUESTA
        $rootScope.extraerTag = function(propuesta) {
            var res = null;
            var tagIndex = propuesta.description.lastIndexOf("#");
            if (tagIndex != -1) { // REVISAR -- si no hay tag y entre los comentarios hay #
                res = propuesta.description.substring(tagIndex + 1);
            }

            /*return "Delicias";*/
            return res;
        };

        $scope.criteriaMatch = function() {
            return function(poll) { 
               

                if ($rootScope.barriosFiltrados.selectedOption && $rootScope.barriosFiltrados.selectedOption !="Toda la ciudad") {

                    var tag = $scope.extraerTag(poll);
                    console.log("tag = " + tag);
                    if (tag) {
                        return ($rootScope.barriosFiltrados.selectedOption.indexOf(tag) > -1);
                    } else {
                        return false;
                    }

                } else { // si no tiene filtrado por barrio
                    return true;
                }

            };
        };




        // Close panel
        $rootScope.closePanel = function(panelId) {
            if (isWebKit()) {
                document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
            } else {
                document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
            }
            document.getElementById(panelId).style.zIndex = 999;

            if (panelId == 'details') {

                //tamaño contenido detalle
                $rootScope.emSizeContenido = 14.2 * parseFloat($("body").css("font-size"));
                $("#details .contenido").height("100%").height("-=" + $rootScope.emSizeContenido + "px");

                $rootScope.closeVoteOption();
                $rootScope.optionSelected = null;
            }

            if (panelId == "newpoll") {
                $rootScope.barrios.selectedOption = $rootScope.barriosFiltrados.selectedOption;
            };
        };


        // Closes all panels
        $rootScope.closeAllPanels = function() {
            $rootScope.closePanel('mypolls');
            $rootScope.closePanel('categories');
            $rootScope.closePanel('results');
            $rootScope.closePanel('details');
            $rootScope.closePanel('pollList');
            $rootScope.closePanel('login');
            $rootScope.closePanel('newpoll');
            $rootScope.closePanel('configuracion');
        };

        $rootScope.showPanel = function(panelId) {
            console.log("menuPanel.showPanel - " + panelId);
            //			$rootScope.fade_back();

            // no cerrar todos los paneles para que al darle al boton volver permanezca abierto por detras
            if (panelId != "configuracion" && panelId != "new_user" && panelId != "results" && panelId != "details" && !$rootScope.estaCategoriaAbierta /*&& panelId != "pollList"*/ ) {
                $rootScope.closeAllPanels();
            }


            if (isWebKit()) {
                document.getElementById(panelId).style.webkitTransform = "translate3d(0,0,0)";
            } else {
                document.getElementById(panelId).style.transform = "translate3d(0,0,0)";
            }
            document.getElementById(panelId).style.zIndex = 200;
            hideKeyboard();
        };

        $rootScope.showResults = function(poll) {
            $rootScope.optionSelected = "aaaa";
            $rootScope.calculateData(poll);
            $rootScope.showPanel('results');
        };

        $rootScope.calculateData = function(poll) {

            $rootScope.selectedPoll = poll;

            $rootScope.aportaciones = [];
            $rootScope.labels = [];
            $rootScope.data = [];
            if (poll.votosAFavor == null) {
                $rootScope.selectedPoll.votosAFavor = 0;
            }
            if (poll.votosEnContra == null) {
                $rootScope.selectedPoll.votosEnContra = 0;
            }


            if (!$rootScope.logged) {
                votingService.getAportacionesDePropuesta(poll.id, function(data) {
                    console.log("Aportaciones:" + data.totalCount);
                    $rootScope.aportaciones = data.result;
                });
            } else {
                votingService.getAportacionesDePropuestaUsuario(poll.id, $rootScope.accountId, function(data) {
                    console.log("Aportaciones usuario:" + data.totalCount);
                    $rootScope.aportaciones = data.result;
                });

            }

            $rootScope.totalVotos = $rootScope.selectedPoll.votosAFavor + $rootScope.selectedPoll.votosEnContra;
            if ($rootScope.totalVotos != 0) {
                $rootScope.porcentajeAFavor = Math.round(poll.votosAFavor * 100 / $rootScope.totalVotos);
                $rootScope.porcentajeEnContra = Math.round(poll.votosEnContra * 100 / $rootScope.totalVotos);

            } else {
                $rootScope.porcentajeAFavor = 0;
                $rootScope.porcentajeEnContra = 0;

            }

            $rootScope.data.push($rootScope.selectedPoll.votosAFavor);
            $rootScope.data.push($rootScope.selectedPoll.votosEnContra);
            $rootScope.labels.push("A Favor");
            $rootScope.labels.push("En Contra");

        };

        $rootScope.showDetails = function(poll) {
            $rootScope.optionSelected = null;
            $rootScope.calculateData(poll);
            $rootScope.showPanel('details');

            $rootScope.marcarVoto();
        };


        // Devuelve si la propuesta esta cerrada
        $rootScope.isClosed = function(pollEnd) {
            if (angular.isDefined(pollEnd)) {
                var now = new Date;
                var endTime = new Date(pollEnd);
                return (now > endTime);
            } else {

                return false;
            }
        };


        // Marcar voto en el detalle
        $rootScope.marcarVoto = function() {

            console.log("votoPoll = " + $rootScope.selectedPoll.votoUsuario);

            if ($rootScope.selectedPoll.votoUsuario) {
                if ($rootScope.selectedPoll.votoUsuario == "positivo") {
                    $(".opcionSelec").html("a-favor");
                } else if ($rootScope.selectedPoll.votoUsuario == "negativo") {
                    $(".opcionSelec").html("en-contra");
                } else {
                    $(".opcionSelec").html($rootScope.selectedPoll.votoUsuario);
                }
            } else {
                $(".opcionSelec").html("");
            }
        };

        $rootScope.showPollopt = function(poll, index) {
            $(".votar").removeClass("slideIn");
            $("#indexpollvotar_" + index).addClass("slideIn");
            console.log("mostrar slideIn " + index);
        };

        $rootScope.contieneTagFiltro = function(poll) {

                if ($rootScope.barriosFiltrados.selectedOption && $rootScope.barriosFiltrados.selectedOption !="Toda la ciudad") {

                    var tag = $scope.extraerTag(poll);
                    
                    if (tag) {
                    	console.log("contieneTagFiltro = " + tag);
                        return ($rootScope.barriosFiltrados.selectedOption.indexOf(tag) > -1);
                    } else {
                        return false;
                    }

                } else { // si no tiene filtrado por barrio
                    return true;
                }

            
        };

        $rootScope.contarCategorias = function() {
        	$rootScope.numPropuestas = 0;
        	$rootScope.numPlanes = 0;
        	$rootScope.numEncuestas = 0;
        	$rootScope.numOtros = 0;

            $rootScope.encuestasPropuesta = [];
            $rootScope.encuestasPlan = [];
            $rootScope.encuestasEncuesta = [];
            $rootScope.encuestasOtros = [];
            $rootScope.encuestasTotales = [];
            jQuery.each($rootScope.encuestas, function(i, val) {
               console.log("category:" + val.category);


               if ($rootScope.contieneTagFiltro(val)) { // si contiene nuestro tag o es "Toda la ciudad"

                   if (val.category == "Propuesta") {
                       $rootScope.numPropuestas = $rootScope.numPropuestas + 1;
                       $rootScope.encuestasPropuesta.push(val);
                   } else if (val.category == "Plan Director") {
                       $rootScope.numPlanes = $rootScope.numPlanes + 1;
                       $rootScope.encuestasPlan.push(val);
                   } else if (val.category == "Encuesta Ciudadana") {
                       $rootScope.numEncuestas = $rootScope.numEncuestas + 1;
                       $rootScope.encuestasEncuesta.push(val);
                   } else {
                       $rootScope.numOtros = $rootScope.numOtros + 1;
                       $rootScope.encuestasOtros.push(val);
                   }

               };



           });

            $rootScope.encuestasTotales = $rootScope.encuestas;
        };

        $rootScope.updatePolls = function() {
            $rootScope.numPropuestas = 0;
            $rootScope.numPlanes = 0;
            $rootScope.numEncuestas = 0;
            $rootScope.numOtros = 0;

            if ($rootScope.logged) {
                console.log("LOGGED");
                votingService.getPropuestasConVoto($rootScope.accountId, function(data) {
                    //					console.log("TODAS LAS PROPUESTAS::::"+JSON.stringify(data));		
                    console.log("Número de propuestas con Voto:" + data.totalCount);
                    $rootScope.encuestas = data.result;
                    $scope.contarCategorias();

                });
            } else {
                console.log("NO LOGGED");
                votingService.getPropuestas(function(data) {
                    console.log("Número propuestas:" + data.totalCount);
                    $rootScope.encuestas = data.result;
                    $scope.contarCategorias();

                });
            }

            $(".votar").addClass("slideOut");
        };

        $rootScope.filterByCategory = function(category) {


        };

        $rootScope.clickMenuIcon = function() {
            $("#menu").addClass("open");

        };

        $rootScope.clickBackOn = function(idPanel) {
            if (idPanel == "pollList") {
                $rootScope.estaCategoriaAbierta = false;
            };
            $rootScope.closePanel(idPanel);


        };

        $rootScope.clickMenu = function() {

            $("#menu").removeClass("open");
        };

        $rootScope.updateMyPolls = function() {

            votingService.getPropuestasDeUsuario($rootScope.accountId, function(data) {
                console.log("Número mis propuestas:" + data.totalCount);
                $rootScope.mypolls = data.result;
            });
            $(".votar").addClass("slideOut");

        };

        //logged and accountId initialization 
        $rootScope.logged = false;
        $rootScope.accountId = "";
/*        		$rootScope.logged = true;
        		$rootScope.accountId = "16354053-e685-bad8-0fdb-44497e2ec1c0";*/

        //polls initialization
        $rootScope.encuestas = [];
        $rootScope.updatePolls();

        //tamaño poll list
        $rootScope.alturaHeader = $("#pollList header").height();
        $("#pollList .lista_poll").height("100%").height("-=" + $rootScope.alturaHeader + "px");

    }
]);

/** Results controller DETALLE*/
iescitiesAppControllers.controller('resultsCtrl', ['$rootScope', '$scope', 'ApiService', 'votingService', '$window',
    function($rootScope, $scope, ApiService, votingService, $window) {

        		// FACEBOOK
			console.log('check facebookInstalled');
			appAvailability.check("com.facebook.katana", // Package Name
					function() { // Success callback
				console.log('FACEBOOK AVAILABLE');
				$rootScope.facebookInstalled = true;
				return true;
					}, function() { // Error callback
						console.log('FACEBOOK NOT available');
						$rootScope.facebookInstalled = false;
					}
			);
			
			// TWITTER
			console.log('check TWITTER');
			appAvailability.check("com.twitter.android", // Package Name
					function() { // Success callback
				console.log('TWITTER AVAILABLE');
				$rootScope.twitterInstalled = true;
				return true;
					}, function() { // Error callback
						console.log('TWITTER NOT available');
						$rootScope.twitterInstalled = false;
					}
			);
			
			
			// WHATSAPP
			console.log('check WHATSAPP');
			appAvailability.check("com.whatsapp", // Package Name
					function() { // Success callback
				console.log('WHATSAPP AVAILABLE');
				$rootScope.whatsappInstalled = true;
				return true;
					}, function() { // Error callback
						console.log('WHATSAPP NOT available');
						$rootScope.whatsappInstalled = false;
					}
			);



        $rootScope.optionSelected = null;
        // parametros para animacion setOption (Results controller)
        //	 	$rootScope.voteOptionHeight = $(".voteOption").height();		  
        $rootScope.voteOptionPosition = $(".voteOption").position();

        $scope.voteAportacion = function(aportacionId, voto) {
            console.log("Vote Aportacion: " + aportacionId + " " + voto);
            votingService.voteAportacion(aportacionId, voto, $rootScope.accountId)
                .success(
                    function(data) {
                        // Actualizar aportaciones			
                        votingService.getAportacionesDePropuestaUsuario($rootScope.selectedPoll.id, $rootScope.accountId, function(data) {
                            console.log("Aportaciones JSON: " + JSON.stringify(data));
                            $rootScope.aportaciones = data.result;
                        });
                        toastr.info("Su voto ha sido emitido con &eacute;xito y será registrado en unos momentos");
                    })
                .error(
                    function(data) {
                        toastr.error("Se ha producido un error.");
                    });

        };

        $scope.createAportacion = function() {
            console.log("Crear Aportacion: " + $scope.newaportacion.comment);
            votingService.createAportacion($rootScope.accountId, $rootScope.selectedPoll.id, $scope.newaportacion, function() {
                $rootScope.showPanel('details');
            });

        };

        $scope.message = function(msg) {
            toastr.info(msg);
        };

        $scope.showNewAportacion = function() {
            if (!$rootScope.logged) {
                toastr.info("Debe logarse para poder crear una aportación");
            } else {
                console.log("show Aportacion: ");
                $("#newaportacion").show();
                $("#shownewaportacion").hide();
                $("#sendaportacion").show();
            }

        };

        $scope.cancelAportacion = function() {
            console.log("cancel Aportacion: ");
            $("#newaportacion").hide();
            $("#shownewaportacion").show();
            $("#sendaportacion").hide();

        };

        $scope.showDetails = function() {
            console.log("En el showDetails");
            $rootScope.optionSelected = null;
            $rootScope.marcarVoto();
            $rootScope.showPanel('details');
            $rootScope.closePanel("results");
        };

        $scope.tapEvent = function() {
            console.log("desplegar opciones");
            $(".voteOption").height("auto");
            $(".voteOption").css("top", top);
            // crearTapEvent();
            $(".voteOption").animate({
                top: "9em",
                bottom: "0",
                height: "auto"
            }, 300, function() {
                // Animation complete.
            }).css("background-image", "none");
            $("#vota").hide();
            $(".opcionSelec").hide();
            $(".encuestaDetalle").hide();
            $(".encuestaOpciones").show();
        };

        $scope.showOption = function(option) {
            console.log("showOPtion()" + option.votes + "/" + $rootScope.selectedPoll.votes);
            $scope.showOption = option;
            $scope.percentage = Math.round(option.votes * 100 / $rootScope.selectedPoll.votes);
        };

        $scope.showResults = function() {

            console.log("En el Detalle");
            $rootScope.optionSelected = null;
            $rootScope.showResults($rootScope.selectedPoll);
            $rootScope.closePanel("details");
        };

        $scope.setOption = function(option) {
            //		  $scope.altura = 100;
            //		  $scope.posicion = 100 + $(".botonera").position();
            //		  $scope.top = 610;
            //		  $scope.alturamapa = $(".mapaInfo").height();
            console.log("Selected Option:" + option);
            $rootScope.optionSelected = option;
            $(".opcionSelec").html(option);

            $scope.closeVoteOption();

        };

        $rootScope.closeVoteOption = function() {
            $(".voteOption").animate({
                top: $rootScope.voteOptionPosition.top,
                bottom: "4em",
                height: "6em"
            }, 300, function() {
                // Animation complete.
            }).css("background-image", "url(img/fondo_voteOption.png)");
            //      	    $(".voteOption").prepend("<p class='opcionSelec' content='a'>"+$rootScope.optionSelected);
            $(".encuestaDetalle").show();
            //      	    $("#vota").show().children("img").hide();
            $("#vota").show()
            $(".encuestaOpciones").hide();
            //      	    $(".setVote").removeClass("disable");
            $(".botonera").show();
            $(".opcionSelec").show();
        };


        $scope.setVote = function() {
            console.log("Voting for :" + $rootScope.selectedPoll.id + " " + $rootScope.optionSelected);

            votingService.votePoll($rootScope.selectedPoll.id, $rootScope.optionSelected, $rootScope.accountId)
                .success(
                    function(data) {
                        toastr.info("Su voto se ha emitido correctamente y será registrado en unos momentos");

                        // actualizar datos con la respuesta

                        //					$rootScope.oldSelectedPoll = $rootScope.selectedPoll;
                        $rootScope.selectedPoll.votoUsuario = $rootScope.optionSelected;
                        if ($rootScope.optionSelected === "a-favor") {
                            $rootScope.selectedPoll.votosAFavor++;
                        } else {
                            $rootScope.selectedPoll.votosEnContra++;
                        }
                        //					$rootScope.selectedPoll.votosAFavor = ;
                        //					$rootScope.selectedPoll.votosAFavo = ;

                        console.log("VOTO === " + $rootScope.optionSelected);

                        console.log(JSON.stringify($rootScope.selectedPoll));

                        //					var old = JSON.stringify($rootScope.encuestas).replace(JSON.stringify($rootScope.oldSelectedPoll), JSON.stringify($rootScope.selectedPoll));
                        //					$rootScope.encuestas = JSON.parse(old);

                        $rootScope.optionSelected = null;


                        $rootScope.calculateData($rootScope.selectedPoll);

                    })
                .error(
                    function(data) {
                        toastr.error("Ha ocurrido un error");

                        console.log("VOTO === " + $rootScope.optionSelected);
                        //						$rootScope.selectedPoll.votoUsuario = null;
                        //						$rootScope.optionSelected = null; //
                    });
        };


        $rootScope.notLoggedIn = function() {
            toastr.info("Debe logarse para poder votar");
        };

        $rootScope.alreadyVoted = function() {
            toastr.info("Ya ha votado en esta propuesta");
        }

        $rootScope.noSelection = function() {
            toastr.info("Seleccione una opción");
        }
        $rootScope.cerrada = function() {
            toastr.error("La votacion de la propuesta está cerrada");
        }





        /*social shara*/
        $scope.shareTwitter = function(texto) {
            console.log("shareTwitter");
            $window.plugins.socialsharing.shareViaTwitter(texto)
                .then(function(result) {
                    // Success!
                    toastr
                        .info('Mensaje enviado correctamente!');
                }, function(err) {
                    // An error occurred. Show a message to the user
                    toastr
                        .error('Se ha producido un error');
                });

        };




        $scope.shareFacebook = function(texto) {
            console.log("shareFacebook");
            $window.plugins.socialsharing.shareViaFacebook(texto)
                .then(function(result) {
                    // Success!
                    toastr
                        .info('Mensaje enviado correctamente!');
                }, function(err) {
                    // An error occurred. Show a message to the user
                    toastr
                        .error('Se ha producido un error');
                });
        };


        $scope.shareWhatsapp = function(texto) {

            $window.plugins.socialsharing.shareViaWhatsApp(texto)
                .then(function(result) {
                    // Success!
                    toastr
                        .info('Mensaje enviado correctamente!');
                }, function(err) {
                    // An error occurred. Show a message to the user
                    toastr
                        .error('Se ha producido un error');
                });

        };

        $scope.shareGlobal = function(texto) {

            $window.plugins.socialsharing.share(texto)
                .then(function(result) {
                    // Success!
                    toastr
                        .info('Mensaje enviado correctamente!');
                }, function(err) {
                    // An error occurred. Show a message to the user
                    toastr
                        .error('Se ha producido un error');
                });
        };

    }
]);

/** Categories controller */
iescitiesAppControllers.controller('categoriesCtrl', ['$rootScope', '$scope', 'ApiService',
    function($rootScope, $scope, ApiService) {

        $scope.categories = [];

        ApiService.getCategories(function(data) {
            console.log("Categories" + data);
            $scope.categories = data;
        });

        $scope.viewPolls = function(category) {
            console.log("Show Polls filtered by:" + category);
            if (category == "propuesta") {
                $rootScope.encuestas = $rootScope.encuestasPropuesta;
            } else if (category == "plan") {
                $rootScope.encuestas = $rootScope.encuestasPlan;

            } else if (category == "encuesta") {
                $rootScope.encuestas = $rootScope.encuestasEncuesta;

            } else if (category == "otros") {
                $rootScope.encuestas = $rootScope.encuestasOtros;

            }

            $rootScope.estaCategoriaAbierta = true; // booleano para controlar si la vista categorias esta abierta

            document.getElementById("categories").style.zIndex = 100;
            $rootScope.showPanel("pollList"); // abrir pollList sin cerrar la vista de categorias
        };

    }
]);

/** NewPoll controller */
iescitiesAppControllers.controller('newpollCtrl', ['$rootScope', '$scope', 'ApiService', 'votingService',
    function($rootScope, $scope, ApiService, votingService) {

        $scope.newpoll = null;


        $rootScope.barrios = {
            availableOptions: ["Toda la ciudad"],
            selectedOption: "Toda la ciudad"
        };
        $rootScope.barrios.availableOptions = $rootScope.barriosZaragoza;
        // $rootScope.barrios.selectedOption = $rootScope.barriosFiltrados.selectedOption;


        $scope.createPoll = function() {
            if ($scope.newpollForm.$valid) {

                var barrioTag = "#" + $rootScope.barrios.selectedOption;

                if ($rootScope.barrios.selectedOption !="Toda la ciudad") {
                	$scope.newpoll.description = $scope.newpoll.description + " " + barrioTag;
                };

                console.log($scope.newpoll.description);

                votingService.createPoll($scope.accountId,$scope.newpoll);
                $scope.newpoll = null;
                $rootScope.showPanel("pollList");
            } else {
                if ($scope.newpollForm.title.$invalid) {
                    toastr.error("El nombre debe tener entre 10 y 200 caracteres");
                } else {
                    toastr.error("La descripción debe tener entre 10 y 500 caracteres");
                }
            }

        };


    }
]);

/** Nav controller */
iescitiesAppControllers.controller('navCtrl', ['$rootScope', '$scope', 'ApiService',
    function($rootScope, $scope, ApiService) {
        $scope.openCategories = function() {
            console.log("openCategories");
            $rootScope.showPanel('categories');
            // $rootScope.estaCategoriaAbierta = true; // booleano para controlar si la vista categorias esta abierta
        };

        $scope.openMypolls = function() {
            $rootScope.showPanel('mypolls');
        };

        $scope.openLogin = function() {
            $rootScope.showPanel('login');
        };

        $scope.openList = function() {
            $rootScope.encuestas = $rootScope.encuestasTotales;
            $rootScope.showPanel('pollList');
        };

        $scope.openNewpoll = function() {
            $rootScope.showPanel('newpoll');
        };


    }
]);

/** My Polls controller */
iescitiesAppControllers.controller('mypollsCtrl', ['$rootScope', '$scope', 'ApiService', 'votingService',
    function($rootScope, $scope, ApiService, votingService) {

        $rootScope.mypolls = [];

        $rootScope.mypolls = $rootScope.updateMyPolls();

        $rootScope.showMyPollopt = function(poll, index) {
            $(".votar").removeClass("slideIn");
            $("#indexmypollvotar_" + index).addClass("slideIn");
            console.log("mostrar slideIn " + index);
        };



    }
]);

/** Profile controller */
iescitiesAppControllers
    .controller(
        'profileCtrl', [
            '$rootScope',
            '$scope',
            '$http',
            'profileInfoService',
            function($rootScope, $scope, $http, profileInfoService) {

                // Close panel
                $scope.closePanel = function(panelId) {
                    if (isWebKit()) {
                        document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
                    } else {
                        document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
                    }
                    document.getElementById(panelId).style.zIndex = 999;
                };

                // Creates new user
                $scope.createNewUser = function() {



                    if ($scope.profileData == null) {
                        toastr
                            .info("El nombre, el correo y el password son obligatorios");
                        console.log($scope.profileData);
                        return;
                    }

                    if ($scope.profileData.name == null) {
                        toastr.info("El nombre es obligatorio");
                        return;
                    }

                    if ($scope.profileData.email == null) {
                        toastr
                            .info("No has introducido el correo electr��nico");
                        return;
                    }

                    if ($scope.profileData.password == null) {
                        toastr
                            .info("No has introducido el password");
                        return;
                    }

                    if ($scope.profileData.passwordConfirm == null) {
                        toastr
                            .info("No has confirmado el password");
                        return;
                    }

                    if ($scope.profileData.password != $scope.profileData.passwordConfirm) {
                        toastr.info("El password no coincide");
                        return;
                    }

                    //								var schoolType = false;
                    //								if ($scope.profileData.school == false
                    //										|| $scope.profileData.school == undefined) {
                    //									schoolType = false;
                    //								} else {
                    //									schoolType = true;
                    //								}

                    var profileBody = {
                        "name": $scope.profileData.name,
                        "email": $scope.profileData.email,
                        "password": $scope.profileData.password,
                        // nif opcional
                        "nif": $scope.profileData.nif
                    };

                    console.log(profileBody);



                    profileInfoService.createNewUser(profileBody)
                        .success(
                            function(data) {
                                toastr
                                    .info("Se ha registrado correctamente");
                                $scope
                                    .closePanel('new_user');
                            })
                        .error(
                            function(data) {
                                toastr
                                    .error("Error al crear usuario: " + data.mensaje);
                            });


                };

                // Updated current user profile
                //							$scope.updateProfile = function() {
                //
                //								var profileBody = {
                //									"name" : $scope.profileData.name,
                //									"email" : $scope.profileData.email,
                //									"password" : $scope.profileData.password,
                //									// nif opcional
                //									"nif" : $scope.profileData.nif
                //								};
                //
                //								if ($scope.profileData == null) {
                //									toastr
                //											.info("El correo y el password son obligatorios");
                //									return;
                //								}
                //
                //								if ($scope.profileData.email == null) {
                //									toastr
                //											.info("No has introducido el correo electr��nico");
                //									return;
                //								}
                //
                //								if ($scope.profileData.password == null) {
                //									toastr
                //											.info("No has introducido el password");
                //									return;
                //								}
                //
                //								if ($scope.profileData.currentPassword != $scope.profileData.password) {
                //									toastr
                //											.info("El password actual no es v��lido");
                //									return;
                //								}
                //
                //								if ($scope.profileData.newPassword != undefined
                //										&& $scope.profileData.newPassword.length > 0) {
                //									profileBody.password = $scope.profileData.newPassword;
                //									$scope.profileData.password = $scope.profileData.newPassword;
                //								}
                //
                //							};

                $scope.closePanelWithId = function(panelId) {
                    $scope.closePanel(panelId);
                };

            }
        ]);

/** Login controller */
iescitiesAppControllers
    .controller(
        'loginCtrl', ['$scope',
            '$rootScope',
            '$http',
            'loginService',
            'votingService',
            function($scope, $rootScope, $http, loginService, votingService) {

                // Close panel
                $scope.closePanel = function(panelId) {
                    if (isWebKit()) {
                        document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
                    } else {
                        document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
                    }
                    document.getElementById(panelId).style.zIndex = 999;
                };

                $scope.rememberLoginData = window.localStorage
                    .getItem("rememberLoginData") == 'true' ? true : false;

                // Load stored account credential values
                $scope.loginData = {
                    email: window.localStorage.getItem("email"),
                    password: window.localStorage.getItem("password")
                };

                $scope.userLogin = function() {
                    loginService.login($scope.loginData, function(data) {
                        console.log(JSON.stringify(data));
                        //load my proposals
                        $rootScope.updateMyPolls();
                        //update polls with users votes
                        $rootScope.updatePolls();
                        $rootScope.showPanel("pollList");

                    });

                };

                $scope.openNewUserPanel = function() {
                    $scope.showPanel('new_user');
                }


                $scope.closePanelWithId = function(panelId) {
                    $scope.closePanel(panelId);
                };

            }
        ]
    );

/**
 * Controller - Privacy
 * 
 * Politica de privacidad
 * 
 */
iescitiesAppControllers.controller('privacyCtrl', [
    '$rootScope',
    '$scope',
    function($rootScope, $scope) {

        $rootScope.privacyVisible = false;

        var isPrivacyAccepted = window.localStorage
            .getItem("isPrivacyAccepted");

        if (isPrivacyAccepted) {
            $rootScope.privacyVisible = false;
        } else {
            $rootScope.privacyVisible = true;
        }

        $scope.acceptPrivacy = function() {
            $rootScope.privacyVisible = false;
            window.localStorage.setItem("isPrivacyAccepted", true);
        };

        $scope.refusePrivacy = function() {
            navigator.app.exitApp();
        };

    }
]);



/**
 * 
 * 
 * Configuracion - Lista de barrios a filtrar
 * 
 */
iescitiesAppControllers.controller('ConfiguracionCtrl', [
    '$rootScope',
    '$scope',
    function($rootScope, $scope) {


        $rootScope.openConfig = function() {
            $scope.showPanel('configuracion');
        }


        $scope.closePanelWithId = function(panelId) {
            $scope.closePanel(panelId);
        };


        $scope.$watch('barriosFiltrados.selectedOption', function() {
            console.log('hay cambios en filtros');

            $rootScope.barrios.selectedOption = $rootScope.barriosFiltrados.selectedOption;
            $rootScope.contarCategorias();
            window.localStorage.setItem("barrioSeleccionado", JSON.stringify($rootScope.barriosFiltrados.selectedOption));

        }, true);

    }
]);



//controller charts
iescitiesAppControllers.controller("DoughnutCtrl", function($scope) {
    $scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
    $scope.data = [300, 500, 100];
});
