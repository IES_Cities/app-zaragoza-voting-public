'use strict';

/* Services */
var iescitiesAppServices = angular.module('iescitiesAppServices', []);

/** Cordova ready listener */
iescitiesAppServices.factory('cordovaReady', function() {
	return function(fn) {

		var queue = [];

		var impl = function() {
			queue.push(Array.prototype.slice.call(arguments));
		};

		document.addEventListener('deviceready', function() {
			queue.forEach(function(args) {
				fn.apply(this, args);
			});
			impl = fn;
		}, false);

		return function() {
			return impl.apply(this, arguments);
		};
	};
});


/** Geolocation service */
iescitiesAppServices.factory('geolocation', [
		'$rootScope',
		'cordovaReady',
		function($rootScope, cordovaReady) {
			return {
				getCurrentPosition : function(onSuccess, onError, options) {
					var success = function() {
						var that = this, args = arguments;

						if (onSuccess) {
							$rootScope.$apply(function() {
								onSuccess.apply(that, args);
							});
						}
					}, error = function() {
						var that = this, args = arguments;
						if (onError) {
							$rootScope.$apply(function() {
								onError.apply(that, args);
							});
						}
					};
					if (document.URL.indexOf('http://') === -1
							&& document.URL.indexOf('https://') === -1
							&& document.URL.indexOf('file://') === -1) {
						// PhoneGap application
						cordovaReady(function(onSuccess, onError, options) {
							navigator.geolocation.getCurrentPosition(success,
									error, options);
						});
					} else if (navigator.geolocation) {
						// Web page
						navigator.geolocation.getCurrentPosition(success,
								error, options);
					}
				}
			};
		} ]);


/** API Service */
/** Service that uses the IES Cities server as data storage**/
iescitiesAppServices
		.factory(
				'ApiService',
				[
						'$http',
						'$rootScope',
						'$log',						
						function($http, $rootScope, $log) {
							return {

								// GET: get Polls Static
								getPollsStatic : function(callback) {
									$log.debug("ApiService.getPolls()");	
									
										return $http.get('scripts/iescities/data/getPolls.json').success(callback);
								},
								
								// GET: get Categories Static
								getCategories : function(callback) {
									$log.debug("ApiService.getCategories()");	
									
										return $http.get('scripts/iescities/data/getCategories.json').success(callback);
								},
							
							// POST: get polls
							getPolls : function(callback) {
								$log.debug("ApiService.getPolls() - Service");	
								$.ajax ({
							        url: 'https://iescities.com/IESCities/api/data/query/252/sql',
							        type: "POST",
							        data: 'select * from poll',
							        beforeSend: function (xhr){
							            xhr.setRequestHeader('Authorization', "Basic " + btoa("zgz_voting:RewSEx7dD8H0"));
							        },
							        contentType: 'text/plain',
							        }
							    ).done(function(json) {
							        console.log(JSON.stringify(json));
							        console.log("---------------");
							        callback(json);
							        
							    }).error(function(json) {
							        console.log(JSON.stringify(json));
							        console.log("---------------");
							        callback(json);
							        
							    });
								
													
							},
								
								// POST: get options
								getOptions : function(pollId,callback) {
									$log.debug("ApiService.getOptions() "+pollId);	
									$.ajax ({
								        url: 'https://iescities.com/IESCities/api/data/query/252/sql',
								        type: "POST",
								        data:  'select * from Option where poll = "'+pollId+'"',
								        beforeSend: function (xhr){
								            xhr.setRequestHeader('Authorization', "Basic " + btoa("zgz_voting:RewSEx7dD8H0"));
								        },
								        contentType: 'text/plain',
								        }
								    ).done(function(json) {
								        console.log(JSON.stringify(json));
								        console.log("---------------");
								        callback(json);
								        
								    });
					
								},
								// POST: insert new poll
								insertPoll : function(poll,callback) {
									
									$log.debug("ApiService.insertPoll()");					
									$.ajax ({
								        url: 'https://iescities.com/IESCities/api/data/update/252/sql?transaction=true',
								        type: "POST",
								        data: 'insert into poll(id,name,description,votes,startDate,enData, type, status, author) values (NULL,"'+poll.name+'","'+poll.description+'",0,"'+new Date().toString('yyyy-MM-dd hh:mm:ss')+'","'+new Date().toString('yyyy-MM-dd hh:mm:ss')+'","'+poll.type+'","abierta","'+poll.author+'");'+
							        	      'insert into Option(id,name,description,votes,poll) select NULL,"a","A Favor",0, id from poll where name="'+poll.name+'";'+
							        	      'insert into Option(id,name,description,votes,poll) select NULL,"b","En Contra",0, id from poll where name="'+poll.name+'"',
								        beforeSend: function (xhr){
								            xhr.setRequestHeader('Authorization', "Basic " + btoa("zgz_voting:RewSEx7dD8H0"));
								        },
								        contentType: 'text/plain',
								        }
								    ).done(function(json) {
								        console.log(JSON.stringify(json));
								        callback(json);
								    }).error(function(json) {
								        console.log(JSON.stringify(json));
								        console.log("---------------");
								        callback("error");
								        
								    });
																							
								},
								insertVote : function(callback) {
									$log.debug("ApiService.insertVote() ");					
									$.ajax ({
								        url: 'https://iescities.com/IESCities/api/data/update/252/sql?transaction=true',
								        type: "POST",
								        data: 'insert into Uservote (id,user,poll,option) values(NULL,1,'+$rootScope.selectedPoll.id+','+$rootScope.optionSelected.id+');'+
								        	'update option set votes = votes+1 where id='+$rootScope.optionSelected.id+';'+
								        	'update poll set votes = votes+1 where id='+$rootScope.selectedPoll.id,
								        beforeSend: function (xhr){
								            xhr.setRequestHeader('Authorization', "Basic " + btoa("zgz_voting:RewSEx7dD8H0"));
								        },
								        contentType: 'text/plain',
								        }
								    ).done(function(json) {
								        console.log(JSON.stringify(json));
								        callback(json);
								    }).error(function(json) {
								        console.log(JSON.stringify(json));
								        console.log("---------------");
								        callback("error");
								        
								    });
																							
								},
								initPolls : function(callback) {
									$log.debug("ApiService.initPolls() ");					
									$.ajax ({
								        url: 'https://iescities.com/IESCities/api/data/update/252/sql?transaction=true',
								        type: "POST",
								        data: 'delete from Poll where id < "10";delete from option where id < "10";delete from Uservote where id < "10";'+
								        	  'insert into poll(id,name,description,votes,startDate,enData, type, status, author) values (1,"Más autobuses nocturnos en Delicias.","Sería muy util contar con un servicio de autobuses nocturno para el barrio.",0,"'+new Date().toString('yyyy-MM-dd hh:mm:ss')+'","'+new Date().toString('yyyy-MM-dd hh:mm:ss')+'","propuesta","abierta","Ayuntamiento ZGZ");'+
								        	  'insert into poll(id,name,description,votes,startDate,enData, type, status, author) values (2,"Próximo concierto en la ciudad","¿Qué lugar de la ciudad elegirías para el próximo concierto en la ciudad?",0,"'+new Date().toString('yyyy-MM-dd hh:mm:ss')+'","'+new Date().toString('yyyy-MM-dd hh:mm:ss')+'","multiopción","abierta","Ayuntamiento ZGZ");'+
								        	  'insert into Option(id,name,description,votes,poll) values(1,"a","A Favor",0,1);'+
								        	  'insert into Option(id,name,description,votes,poll) values(2,"b","En contra",0,1);'+
								        	  'insert into Option(id,name,description,votes,poll) values(3,"a","Plaza Mayor",0,2);'+
								        	  'insert into Option(id,name,description,votes,poll) values(4,"b","Delicias",0,2);'+
								        	  'insert into Option(id,name,description,votes,poll) values(5,"c","Calle Pinares",0,2);'+
								        	  'insert into Option(id,name,description,votes,poll) values(6,"d","Estadio Futbol",0,2);',
								        beforeSend: function (xhr){
								            xhr.setRequestHeader('Authorization', "Basic " + btoa("zgz_voting:RewSEx7dD8H0"));
								        },
								        contentType: 'text/plain',
								        }
								    ).done(function(json) {
								        console.log(JSON.stringify(json));
								        callback(json);
								    }).error(function(json) {
								        console.log(JSON.stringify(json));
								        console.log("---------------");
								        callback("error");
								        
								    });
																							
								},
								insertOption : function(pollId,callback) {
									$log.debug("ApiService.insertOption()");					
									$.ajax ({
								        url: 'https://iescities.com/IESCities/api/data/update/252/sql',
								        type: "POST",
								        data: 'insert into Option(id,name,description,votes,poll) values(NULL,"c","Plaza Mayor",0,1)',
								        beforeSend: function (xhr){
								            xhr.setRequestHeader('Authorization', "Basic " + btoa("zgz_voting:RewSEx7dD8H0"));
								        },
								        contentType: 'text/plain',
								        }
								    ).done(function(json) {
								        console.log(JSON.stringify(json));
								        callback(json);
								    });
																							
								}
							};
						} ]);

/** Log Service */
iescitiesAppServices
		.factory(
				'iesCitiesService',
				[
						'$http',
						'$rootScope',
						function($http, $rootScope) {
							return {

								// Iescities server app log
								sendLog : function(message, type) {
									var uri = "http://150.241.239.65:8080/IESCities/api/log/app/stamp/";
									var timestamp = Math.floor(new Date()
											.getTime() / 1000);
									var appid = "zgzvoting";
									var session = $rootScope.sessionId;

									uri = uri + timestamp + "/" + appid + "/"
											+ session + "/" + type + "/"
											+ message;
									return $http(
											{
												method : 'GET',
												url : uri,
												headers : {
													'Accept' : 'application/json',
													'Content-Type' : 'application/json;charset=UTF-8'
												}
											})
											.success(
													function(data) {
														console
																.log("iesCitiesService.sendLog() - SUCCESS");
														console
																.log(JSON
																		.stringify(data));
													})
											.error(
													function(data) {
														console
																.log("iesCitiesService.sendLog() - ERROR");
														console
																.log(JSON
																		.stringify(data));
													});
								}

							};
						} ]);

/** ZGZ Voting Service */
/** Service using Zaragoza API for open government*/
iescitiesAppServices.factory('votingService', [
		'$http',
		'$rootScope',
		function($http, $rootScope) {
			return {

				// Get All propuestas
				getPropuestas : function(callback) {
					console.log("votingService.getPropuestas()");
					
					// return $http.get('scripts/iescities/data/propuesta.json').success(callback);
					var uri = "http://www.zaragoza.es/api/recurso/gobierno-abierto/propuesta";
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "GET" + "/api/recurso/gobierno-abierto/propuesta"
							, "UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					$http({
						method : 'GET',
						url : uri,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain;charset=UTF-8'
						}
					}).success(function(data) {
						console.log("getPropuestas() - OK");
						callback(data);
					}).error(function(data,status) {
						console.log("getPropuestas() - ERROR:"+JSON.stringify(status));
						callback(data);
					});
				},
				// Get All propuestas with votes
				getPropuestasConVoto : function(accountId,callback) {
					// return $http.get('scripts/iescities/data/propuesta.json').success(callback);
					console.log("votingService.getPropuestasConVoto()");
					var uri = "http://www.zaragoza.es/api/recurso/gobierno-abierto/propuesta?user_id="+ accountId;
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "GET" + "/api/recurso/gobierno-abierto/propuesta?user_id="+accountId
							, "UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					$http({
						method : 'GET',
						url : uri,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain;charset=UTF-8'
						}
					}).success(function(data) {
						console.log("getPropuestasConVoto() - OK");
						callback(data);
					}).error(function(data) {
						console.log("getPropuestasConVoto() - ERROR");
						callback(data);
					});

				},
				
				// Gets User's polls
				getPropuestasDeUsuario : function(accountId,callback) {
					console.log("getPropuestasDeUsuario"+accountId);
					
					var uri = "http://www.zaragoza.es/api/recurso/gobierno-abierto/propuesta/"+ accountId;
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "GET" + "/api/recurso/gobierno-abierto/propuesta/"+accountId
							, "UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					$http({
						method : 'GET',
						url : uri,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data) {
						console.log("getPropuestasDeUSuario() - OK");
						callback(data);
					}).error(function(data) {
						console.log("getPropuestasDeUSuario() - ERROR");
						toastr.error("El servicio no esta disponible en estos momentos. Por favor inténtelo de nuevo más tarde.");
						callback(data);
					});
				},
			
				// Gets Aportaciones de propuesta
				getAportacionesDePropuesta : function(idPoll,callback) {
					console.log("votingService.getAportacionesDePropuestas()");				
					return $http.get('http://www.zaragoza.es/api/recurso/gobierno-abierto/propuesta/'+idPoll+'/aportacion.json').success(callback);

				},
				// Gets Aportaciones de propuesta con voto de usuario
				getAportacionesDePropuestaUsuario : function(idPoll,accountId,callback) {
					console.log("votingService.getAportacionesDePropuestasUsuario()");		
					var uri = "http://www.zaragoza.es/api/recurso/gobierno-abierto/propuesta/"+idPoll+"/aportacion?user_id="+accountId;
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "GET" + "/api/recurso/gobierno-abierto/propuesta/"+idPoll+"/aportacion?user_id="+accountId
							, "UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					$http({
						method : 'GET',
						url : uri,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data) {
						console.log("getAportacionesDeUSuario() - OK");
						callback(data);
					}).error(function(data) {
						console.log("getAportacionesDeUSuario() - ERROR");
						callback(data);
					});
					

				},
				// Create new Poll
				createPoll: function(accountId,poll) {
					console.log("createPoll() - Voting Service: "+JSON.stringify(poll));
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "POST" + "/api/recurso/gobierno-abierto/propuesta/"+ accountId + JSON.stringify(poll),
							"UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'POST',
						url : 'https://apex-test.zaragoza.es/api/recurso/gobierno-abierto/propuesta/'+accountId,
						data : JSON.stringify(poll),
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain;charset=UTF-8'
						}
					}).success(function(data) {
						Restlogging.appLog(logType.COLLABORATE,"new poll added");						
						 console.log(JSON.stringify(data));
					}).error(function(data,status) {
						 console.log("Error:" + data.error + status.error);
					});
				},
				// Create new Aportacion
				createAportacion: function(accountId,pollId,aportacion,callback) {

					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "POST" + "/api/recurso/gobierno-abierto/propuesta/"+ pollId + "/aportacion/"+accountId + JSON.stringify(aportacion),
							"UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'POST',
						url : 'http://www.zaragoza.es/api/recurso/gobierno-abierto/propuesta/'+ pollId + '/aportacion/'+accountId,
						data : JSON.stringify(aportacion),
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain;charset=UTF-8'
						}
					}).success(function(data) {
						Restlogging.appLog(logType.COLLABORATE,"new contribution added");						
						console.log("Aportación creada");
						toastr.info("Su aportación ha sido creada correctamente");
						callback(data);
					}).error(function(status,data,error) {
						console.log("Error al crear aportación:" + JSON.stringify(error));
						toastr.error("No se ha podido crear la aportación.");
					});
				},
				
				// Vote Poll
				votePoll : function(pollId,voto,accountId) {
					console.log("URI:"+ pollId+" "+voto+" "+accountId);
					var uri = "http://www.zaragoza.es/api/recurso/gobierno-abierto/propuesta/"+ pollId+"/"+voto+"/"+accountId;
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "GET" + "/api/recurso/gobierno-abierto/propuesta/"+ pollId+"/"+voto+"/"+accountId
							, "UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'GET',
						url : uri,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data) {
						console.log("vote OK");
						Restlogging.appLog(logType.COLLABORATE,"poll voted");						
					}).error(function(data) {
						console.log("vote Error");
					});
				},
				
				// Vote Aportacion
				voteAportacion : function(aportacionId,voto,accountId) {

					console.log("URI:"+ $rootScope.selectedPoll.id + aportacionId+" "+voto+" "+accountId);
					var uri = "http://www.zaragoza.es/api/recurso/gobierno-abierto/propuesta/"+ $rootScope.selectedPoll.id+"/aportacion/"+aportacionId+"/"+voto+"/"+accountId;
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "GET" + "/api/recurso/gobierno-abierto/propuesta/"+ $rootScope.selectedPoll.id+"/aportacion/"+aportacionId+"/"+voto+"/"+accountId
							, "UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'GET',
						url : uri,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data) {
						console.log("vote OK");
						Restlogging.appLog(logType.COLLABORATE,"contribution voted");
						
					}).error(function(data,error,status) {
						console.log("status:"+JSON.stringify(status)+"error:"+JSON.stringify(error)+"data"+JSON.stringify(data));
					});
				}
				
			};
		} ]);

/** Login service */
/** User Login service using the Zaragoza API for user management**/
iescitiesAppServices.factory('loginService', [
		'$http',
		'$rootScope',		
		function($http, $rootScope) {
			return {

				// Starts login
				login : function(userData, callback) {
					console.log("login() - loginService "+userData.email+" "+userData.password);
					var login = "email=" + userData.email + "&password=" + userData.password;

					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "POST" + "/api/recurso/users/addentra/" + login,
							"UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");

					console.log("hmac:"+hmac);
					return $http({
						method : 'POST',
						url : 'https://www.zaragoza.es/api/recurso/users/addentra/',
						data : login,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain;charset=UTF-8'
						// 'User-Agent' : 'iescities[movil]',
						// 'Referer' : 'http://www.zaragoza.es/ciudad/'
						}
					}).success(function(data , status, header, config ) {
						console.log("success: accountId="+data.account_id);  
						$rootScope.accountId = data.account_id;
						$rootScope.profileData = data;
						$rootScope.profileData.password = userData.password;
						$rootScope.logged = true;
						Restlogging.appLog(logType.CONSUME, "user logged");							
						toastr.info("Se ha logado correctamente!");
						callback(data);
					}).error(function(data, status, header, config) {
						console.log("Error:"+JSON.stringify(data)+" Status:"+status);
						toastr.error("No se ha podido logar en el sistema.");
					});
				}

			};
		} ]);

/** Profile info service */
iescitiesAppServices.factory('profileInfoService', [
		'$http',
		'$rootScope',
		function($http, $rootScope) {
			return {

				// Get profile info
				getProfileInfo : function(callback) {
					return $http.get('scripts/iescities/data/getProfileInfo.json').success(callback);
				},

				// Create new user
				createNewUser : function(profile, schoolType , callback) {
					
					var json_data = JSON.stringify(profile);
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "POST" + "/api/recurso/users/addentra/new" + json_data,
							"UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'POST',
						url : 'http://www.zaragoza.es/api/recurso/users/addentra/new',
						data : json_data,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain;charset=UTF-8'
						}
					}).success(function(data, status, header, config) {
						console.log("Response: " + JSON.stringify(data));
						$rootScope.account_id = data.account_id;
						$rootScope.profileData = data;
						
//						if (schoolType){
//							Restlogging.appLog(logType.PROSUME,"new user created of type school");	
//						}else {
//							Restlogging.appLog(logType.PROSUME,"new user created");	
//						}						
						
					}).error(function(data) {
						console.log("Response: " + JSON.stringify(data));
					});
				},

				// Update profile info
				updateProfileInfo : function(profile, callback) {
					var uri = "http://www.zaragoza.es/api/recurso/users/addentra/" + $rootScope.account_id;
					var json_data = JSON.stringify(profile);
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "PUT" + "/api/recurso/users/addentra/" + $rootScope.account_id
							+ json_data, "UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'PUT',
						url : uri,
						data : json_data,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data, status, header, config) {
						console.log("Success: " + JSON.stringify(data));
						$rootScope.account_id = data.account_id;
						$rootScope.profileData = data;
					}).error(function(data) {
						console.log("Error: " + JSON.stringify(data));
					});
				}

			};
		} ]);




/**
* SESION DATA SERVICE
*
* Servicio para guardar informacion relativa a la sesion actual para controlar la navegacion.
* Permite compartir informacion entre los controladores.
*
* */
iescitiesAppServices.factory('SesionData', [ '$log' , function ($log) {

   $log.debug("SesionData.init()");
 
   var data = {
       selectedApp: null,       
   };

   return {

       // User name
       getSelectedApp: function () {  
    	   $log.debug("SesionData.getSelectedApp()");
           return data.selectedApp;
       },
       setSelectedApp: function (app) {
           $log.debug("SesionData.setSelectedApp()");
           data.selectedApp = app;
       },

       removeData: function () {
           $log.debug("SesionData.removeData()");
           data = {
        		   selectedApp: null               
           };
           
       }

   };

}]);
