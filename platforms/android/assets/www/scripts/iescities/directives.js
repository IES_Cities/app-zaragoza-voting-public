/* Directives */
var iescitiesAppDirectives = angular.module('iescitiesAppDirectives', []);

/** Main menu directive */
iescitiesAppDirectives
		.directive(
				"pollListPanel",				
				function() {
					return function($scope) {

						// Close panel
						$scope.closePanel = function(panelId) {
							if (isWebKit()) {
								document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
							} else {
								document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
							}
							document.getElementById(panelId).style.zIndex = 999;
						};

						// Closes all panels
						$scope.closeAllPanels = function() {
							$scope.closePanel('mypolls');
							$scope.closePanel('categories');
							$scope.closePanel('results');
							$scope.closePanel('details');
							$scope.closePanel('pollList');
							$scope.closePanel('login');
						};

						// Close all panels and unfade menu
						$scope.fade_back = function() {							
							if (isWebKit()) {
								document.getElementById('main_menu').style.webkitTransform = "translate3d(0,0,0)";
							} else {
								document.getElementById('main_menu').style.left = '-14em';
							}
							document.getElementById('black_fader').style.zIndex = '0';
							document.getElementById('black_fader').style.opacity = '0';
						};

						// Toggles main menu
						$scope.toggle_main_menu = function() {
							if (document.getElementById('black_fader').style.zIndex == ""
									|| document.getElementById('black_fader').style.zIndex == "0") {

								if (isWebKit()) {
									document.getElementById('main_menu').style.webkitTransform = "translate3d(14em,0,0)";
								} else {
									document.getElementById('main_menu').style.left = '0';
								}
								document.getElementById('black_fader').style.zIndex = '990';
								document.getElementById('black_fader').style.opacity = '0.5';

							} else {
								// Ocultar menu
								if (isWebKit()) {
									document.getElementById('main_menu').style.webkitTransform = "translate3d(0,0,0)";
								} else {
									document.getElementById('main_menu').style.left = '-14em';
								}
								document.getElementById('black_fader').style.zIndex = '0';
								document.getElementById('black_fader').style.opacity = '0';

								document.getElementById('edit_map').style.zIndex = '1490';
							}
						};

						$scope.showPanel = function(panelId) {
							console.log("menuPanel.showPanel - " + panelId);
							$scope.fade_back();
							$scope.closeAllPanels();
							if (isWebKit()) {
								document.getElementById(panelId).style.webkitTransform = "translate3d(0,0,0)";
							} else {
								document.getElementById(panelId).style.transform = "translate3d(0,0,0)";
							}
							document.getElementById(panelId).style.zIndex = 200;
							hideKeyboard();
						};

						$scope.addPanel = function(panelId) {
							console.log("menuPanel.addPanel - " + panelId);
							if (isWebKit()) {
								document.getElementById(panelId).style.webkitTransform = "translate3d(0,0,0)";
							} else {
								document.getElementById(panelId).style.transform = "translate3d(0,0,0)";
							}
							document.getElementById(panelId).style.zIndex = 200;
						};

						$scope.popPanel = function(panelId) {
							console.log("menuPanel.popPanel - " + panelId);
							if (isWebKit()) {
								document.getElementById(panelId).style.webkitTransform = "translate3d(100%,0,0)";
							} else {
								document.getElementById(panelId).style.transform = "translate3d(100%,0,0)";
							}
							document.getElementById(panelId).style.zIndex = 200;
						};

					};
				});

iescitiesAppDirectives.directive("resultsPanel", function() {
	return function($scope) {

	};
});


iescitiesAppDirectives.directive("mypollsPanel", function() {
	return function($scope) {
		//start survey
		startRatingSurvey();
		//init logging
		Restlogging.init("https://iescities.com");

	};
});

iescitiesAppDirectives.directive("loginPanel", function() {
	return function($scope) {

	};
});

iescitiesAppDirectives.directive("categoriesPanel", function() {
	return function($scope) {

	};
});
